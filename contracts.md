# Internet Money Deployed Contracts

Below is a list of all currently leveraged Smart Contracts in the Internet Money ecosystem.

## Ethereum (Chain ID 1)
* T.I.M.E. Dividend (TIME): `0xd08481058399490B83a72676901d4e9dB70E75aC`
* FUTURE T.I.M.E. Dividend (FUTURE): `0xd4aE236a5080A09c0f7BD6E6B84919523573a43B`
* Internet Money (IM): `0x0A58153a0CD1cfaea94cE1f7FdC5D7E679eCa936`
* Internet Money Swap Router v3 (DEX Aggregator): `0xF3cb43F19C668e34173D4Bb59E551a2c6E37d778`
* TIME Buy and Burn: `0xa11aA626E637df91f3ccd4f795a3d07a3dFaF00e`
* IM Buy and Burn: `0x2963ab11d012791ACFA7a4b8d428dA129898a8E4`
* HEX Buy and Burn: `0x7E937f7aa4d1Cd02664d4043d439DdB0Cff7dDBE`

## BNB Smart Chain (BSC) (Chain ID 56)
* T.I.M.E. Dividend (TIME): `0x8734022D0fdBF1faeCE14cE077Edfcb936543E25`
* Internet Money (IM): `0xac5D23cE5e4a5eB11A5407a5FBeE201a75E8C8aD`
* Internet Money Swap Router v3 (DEX Aggregator): `0xF3cb43F19C668e34173D4Bb59E551a2c6E37d778`
* TIME Buy and Burn: `0xa11aA626E637df91f3ccd4f795a3d07a3dFaF00e`
* IM Buy and Burn: `0x7E937f7aa4d1Cd02664d4043d439DdB0Cff7dDBE`

## Polygon (Chain ID 137)
* T.I.M.E. Dividend (TIME): `0x9F42bcA1A579fCf9Efc165a0244B12937e18C6A5`
* Internet Money Swap Router v3 (DEX Aggregator): `0x91512b3547C46A1ec9a0385f5731525A036cB032`


## PulseChain (Chain ID 369)
* T.I.M.E. Dividend (TIME): `0xCA35638A3fdDD02fEC597D8c1681198C06b23F58`
* Internet Money (IM): `0xBBcF895BFCb57d0f457D050bb806d1499436c0CE`
* Internet Money Swap Router v3 (DEX Aggregator): `0xF3cb43F19C668e34173D4Bb59E551a2c6E37d778`
* TIME Buy and Burn: `0x1Cb448526DA525Ff0944337e060Cc285e2893E96`
* IM Buy and Burn: `0xf9D7C0e262c59d73B1E8cDdAfaAaef994b0410a3`
* HEX Buy and Burn: `0xB164B80cBE3280501F8133060e9fc671cBBF21A3`
* PEPE Buy and Burn: `0x5a438cf2d76C2232a0CC8dF53baE189DC3d6eE4E`
* AXIS Buy and Burn: `0x3343d9C64B3D1e5321954774e8e8F2E59d616d96`
* HSA Buy and Burn: `0xfaB588612BAc16a9Ce1ED0F23be986b5cE35e88C`
* TEXAN Buy and Burn: `0x9400a853e918f48c5fB40BcC316bBadB2AdA06f0`
* PLSX Buy and Burn: `0xd43af9B689DD92654BFfBe4D158836235aa1c41e`
* INC Buy and Burn: `0x87B8835e9Aa0B303B1D6391b36819c97A2a6BF33`
